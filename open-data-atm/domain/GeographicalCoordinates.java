import lombok.Data;

@Data
public class GeographicalCoordinates {
    private String Latitude;
    private String Longitude;
}
