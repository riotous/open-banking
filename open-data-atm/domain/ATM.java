import lombok.Data;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
public class ATM {
    private String Identification;
    private Set<String> SupportedLanguages;
    private String[] ATMServices;
    private Set<String> Accessibility;
    private boolean Access24HoursIndicator;
    private HashSet<String> SupportedCurrencies;
    private String MinimumPossibleAmount;
    private String Note;
    private List<OtherAccessibility> otherAccessibility;
    private List<OtherATMServices> otherATMServices;
    private Branch branch;
    private Location location;

}
