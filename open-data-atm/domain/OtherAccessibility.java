import lombok.Data;

@Data
public class OtherAccessibility {
    private String Code;
    private String Name;
    private String Description;
}
