import lombok.Data;

@Data
public class OtherLocationCategory {
    private String Code;
    private String Name;
    private String Description;
}
