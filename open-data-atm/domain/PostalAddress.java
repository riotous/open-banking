import lombok.Data;

@Data
public class PostalAddress {
    private String StreetName;
    private String TownName;
    private String Country;
    private String PostCode;
    private GeoLocation geoLocation;
}
