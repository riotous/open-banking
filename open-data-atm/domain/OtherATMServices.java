import lombok.Data;

@Data
public class OtherATMServices {
    private String Code;
    private String Name;
    private String Description;
}
